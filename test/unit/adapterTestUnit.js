/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-openstack_cinder',
      type: 'OpenstackCinder',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const OpenstackCinder = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Openstack_cinder Adapter Test', () => {
  describe('OpenstackCinder Class Tests', () => {
    const a = new OpenstackCinder(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('openstack_cinder'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('openstack_cinder'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('OpenstackCinder', pronghornDotJson.export);
          assert.equal('Openstack_cinder', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-openstack_cinder', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('openstack_cinder'));
          assert.equal('OpenstackCinder', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-openstack_cinder', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-openstack_cinder', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#listAllApiVersions - errors', () => {
      it('should have a listAllApiVersions function', (done) => {
        try {
          assert.equal(true, typeof a.listAllApiVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAPIV3Details - errors', () => {
      it('should have a showAPIV3Details function', (done) => {
        try {
          assert.equal(true, typeof a.showAPIV3Details === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listKnownAPIExtensions - errors', () => {
      it('should have a listKnownAPIExtensions function', (done) => {
        try {
          assert.equal(true, typeof a.listKnownAPIExtensions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listKnownAPIExtensions(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listKnownAPIExtensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAVolumeType - errors', () => {
      it('should have a updateAVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.updateAVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateAVolumeType(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.updateAVolumeType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showVolumeTypeDetail - errors', () => {
      it('should have a showVolumeTypeDetail function', (done) => {
        try {
          assert.equal(true, typeof a.showVolumeTypeDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showVolumeTypeDetail(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showVolumeTypeDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.showVolumeTypeDetail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showVolumeTypeDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAVolumeType - errors', () => {
      it('should have a deleteAVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteAVolumeType(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.deleteAVolumeType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrUpdateExtraSpecsForVolumeType - errors', () => {
      it('should have a createOrUpdateExtraSpecsForVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.createOrUpdateExtraSpecsForVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createOrUpdateExtraSpecsForVolumeType(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createOrUpdateExtraSpecsForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.createOrUpdateExtraSpecsForVolumeType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createOrUpdateExtraSpecsForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAllExtraSpecificationsForVolumeType - errors', () => {
      it('should have a showAllExtraSpecificationsForVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.showAllExtraSpecificationsForVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAllExtraSpecificationsForVolumeType(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAllExtraSpecificationsForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.showAllExtraSpecificationsForVolumeType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAllExtraSpecificationsForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showExtraSpecificationForVolumeType - errors', () => {
      it('should have a showExtraSpecificationForVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.showExtraSpecificationForVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showExtraSpecificationForVolumeType(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showExtraSpecificationForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.showExtraSpecificationForVolumeType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showExtraSpecificationForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.showExtraSpecificationForVolumeType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showExtraSpecificationForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExtraSpecificationForVolumeType - errors', () => {
      it('should have a updateExtraSpecificationForVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.updateExtraSpecificationForVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateExtraSpecificationForVolumeType(null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateExtraSpecificationForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.updateExtraSpecificationForVolumeType('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateExtraSpecificationForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.updateExtraSpecificationForVolumeType('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateExtraSpecificationForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtraSpecificationForVolumeType - errors', () => {
      it('should have a deleteExtraSpecificationForVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtraSpecificationForVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteExtraSpecificationForVolumeType(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteExtraSpecificationForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.deleteExtraSpecificationForVolumeType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteExtraSpecificationForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteExtraSpecificationForVolumeType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteExtraSpecificationForVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showDefaultVolumeType - errors', () => {
      it('should have a showDefaultVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.showDefaultVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showDefaultVolumeType(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showDefaultVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllVolumeTypes - errors', () => {
      it('should have a listAllVolumeTypes function', (done) => {
        try {
          assert.equal(true, typeof a.listAllVolumeTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listAllVolumeTypes('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAllVolumeTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAVolumeType - errors', () => {
      it('should have a createAVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.createAVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createAVolumeType(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAnEncryptionType - errors', () => {
      it('should have a showAnEncryptionType function', (done) => {
        try {
          assert.equal(true, typeof a.showAnEncryptionType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAnEncryptionType(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAnEncryptionType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.showAnEncryptionType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAnEncryptionType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAnEncryptionType - errors', () => {
      it('should have a createAnEncryptionType function', (done) => {
        try {
          assert.equal(true, typeof a.createAnEncryptionType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createAnEncryptionType(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAnEncryptionType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.createAnEncryptionType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAnEncryptionType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showEncryptionSpecsItem - errors', () => {
      it('should have a showEncryptionSpecsItem function', (done) => {
        try {
          assert.equal(true, typeof a.showEncryptionSpecsItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showEncryptionSpecsItem(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showEncryptionSpecsItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.showEncryptionSpecsItem('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showEncryptionSpecsItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.showEncryptionSpecsItem('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showEncryptionSpecsItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnEncryptionType - errors', () => {
      it('should have a deleteAnEncryptionType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnEncryptionType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteAnEncryptionType(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAnEncryptionType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.deleteAnEncryptionType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAnEncryptionType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encryptionId', (done) => {
        try {
          a.deleteAnEncryptionType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'encryptionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAnEncryptionType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnEncryptionType - errors', () => {
      it('should have a updateAnEncryptionType function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnEncryptionType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateAnEncryptionType(null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAnEncryptionType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeTypeId', (done) => {
        try {
          a.updateAnEncryptionType('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'volumeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAnEncryptionType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encryptionId', (done) => {
        try {
          a.updateAnEncryptionType('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'encryptionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAnEncryptionType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPrivateVolumeTypeAccessToProject - errors', () => {
      it('should have a addPrivateVolumeTypeAccessToProject function', (done) => {
        try {
          assert.equal(true, typeof a.addPrivateVolumeTypeAccessToProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.addPrivateVolumeTypeAccessToProject(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-addPrivateVolumeTypeAccessToProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeType', (done) => {
        try {
          a.addPrivateVolumeTypeAccessToProject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-addPrivateVolumeTypeAccessToProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPrivateVolumeTypeAccessDetail - errors', () => {
      it('should have a listPrivateVolumeTypeAccessDetail function', (done) => {
        try {
          assert.equal(true, typeof a.listPrivateVolumeTypeAccessDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listPrivateVolumeTypeAccessDetail(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listPrivateVolumeTypeAccessDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeType', (done) => {
        try {
          a.listPrivateVolumeTypeAccessDetail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'volumeType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listPrivateVolumeTypeAccessDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrUpdateADefaultVolumeType - errors', () => {
      it('should have a createOrUpdateADefaultVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.createOrUpdateADefaultVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createOrUpdateADefaultVolumeType(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createOrUpdateADefaultVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showADefaultVolumeType - errors', () => {
      it('should have a showADefaultVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.showADefaultVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showADefaultVolumeType(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showADefaultVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteADefaultVolumeType - errors', () => {
      it('should have a deleteADefaultVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteADefaultVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteADefaultVolumeType(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteADefaultVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultVolumeTypes - errors', () => {
      it('should have a listDefaultVolumeTypes function', (done) => {
        try {
          assert.equal(true, typeof a.listDefaultVolumeTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAccessibleVolumesWithDetails - errors', () => {
      it('should have a listAccessibleVolumesWithDetails function', (done) => {
        try {
          assert.equal(true, typeof a.listAccessibleVolumesWithDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listAccessibleVolumesWithDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAccessibleVolumesWithDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAVolume - errors', () => {
      it('should have a createAVolume function', (done) => {
        try {
          assert.equal(true, typeof a.createAVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createAVolume(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAccessibleVolumes - errors', () => {
      it('should have a listAccessibleVolumes function', (done) => {
        try {
          assert.equal(true, typeof a.listAccessibleVolumes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listAccessibleVolumes('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAccessibleVolumes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAVolumeSDetails - errors', () => {
      it('should have a showAVolumeSDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showAVolumeSDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAVolumeSDetails(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAVolumeSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.showAVolumeSDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAVolumeSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAVolume - errors', () => {
      it('should have a updateAVolume function', (done) => {
        try {
          assert.equal(true, typeof a.updateAVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateAVolume(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.updateAVolume('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAVolume - errors', () => {
      it('should have a deleteAVolume function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteAVolume('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.deleteAVolume('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMetadataForVolume - errors', () => {
      it('should have a createMetadataForVolume function', (done) => {
        try {
          assert.equal(true, typeof a.createMetadataForVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createMetadataForVolume(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createMetadataForVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.createMetadataForVolume('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createMetadataForVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAVolumeSMetadata - errors', () => {
      it('should have a showAVolumeSMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.showAVolumeSMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAVolumeSMetadata(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAVolumeSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.showAVolumeSMetadata('fakeparam', null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAVolumeSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAVolumeSMetadata - errors', () => {
      it('should have a updateAVolumeSMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.updateAVolumeSMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateAVolumeSMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAVolumeSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.updateAVolumeSMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAVolumeSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAVolumeSMetadataForASpecificKey - errors', () => {
      it('should have a showAVolumeSMetadataForASpecificKey function', (done) => {
        try {
          assert.equal(true, typeof a.showAVolumeSMetadataForASpecificKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAVolumeSMetadataForASpecificKey(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAVolumeSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.showAVolumeSMetadataForASpecificKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAVolumeSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.showAVolumeSMetadataForASpecificKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAVolumeSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAVolumeSMetadata - errors', () => {
      it('should have a deleteAVolumeSMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAVolumeSMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteAVolumeSMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolumeSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.deleteAVolumeSMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolumeSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteAVolumeSMetadata('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolumeSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAVolumeSMetadataForASpecificKey - errors', () => {
      it('should have a updateAVolumeSMetadataForASpecificKey function', (done) => {
        try {
          assert.equal(true, typeof a.updateAVolumeSMetadataForASpecificKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateAVolumeSMetadataForASpecificKey(null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAVolumeSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.updateAVolumeSMetadataForASpecificKey('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAVolumeSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.updateAVolumeSMetadataForASpecificKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAVolumeSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVolumesSummary - errors', () => {
      it('should have a getVolumesSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getVolumesSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getVolumesSummary('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-getVolumesSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#extendAVolumeSize - errors', () => {
      it('should have a extendAVolumeSize function', (done) => {
        try {
          assert.equal(true, typeof a.extendAVolumeSize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.extendAVolumeSize(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-extendAVolumeSize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.extendAVolumeSize('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-extendAVolumeSize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#manageAnExistingVolume - errors', () => {
      it('should have a manageAnExistingVolume function', (done) => {
        try {
          assert.equal(true, typeof a.manageAnExistingVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.manageAnExistingVolume(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-manageAnExistingVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSummaryOfVolumesAvailableToManage - errors', () => {
      it('should have a listSummaryOfVolumesAvailableToManage function', (done) => {
        try {
          assert.equal(true, typeof a.listSummaryOfVolumesAvailableToManage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listSummaryOfVolumesAvailableToManage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listSummaryOfVolumesAvailableToManage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDetailOfVolumesAvailableToManage - errors', () => {
      it('should have a listDetailOfVolumesAvailableToManage function', (done) => {
        try {
          assert.equal(true, typeof a.listDetailOfVolumesAvailableToManage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listDetailOfVolumesAvailableToManage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listDetailOfVolumesAvailableToManage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSnapshotsAndDetails - errors', () => {
      it('should have a listSnapshotsAndDetails function', (done) => {
        try {
          assert.equal(true, typeof a.listSnapshotsAndDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listSnapshotsAndDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listSnapshotsAndDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createASnapshot - errors', () => {
      it('should have a createASnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.createASnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createASnapshot(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAccessibleSnapshots - errors', () => {
      it('should have a listAccessibleSnapshots function', (done) => {
        try {
          assert.equal(true, typeof a.listAccessibleSnapshots === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listAccessibleSnapshots('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAccessibleSnapshots', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showASnapshotSMetadata - errors', () => {
      it('should have a showASnapshotSMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.showASnapshotSMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showASnapshotSMetadata(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showASnapshotSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.showASnapshotSMetadata('fakeparam', null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showASnapshotSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createASnapshotSMetadata - errors', () => {
      it('should have a createASnapshotSMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.createASnapshotSMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createASnapshotSMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createASnapshotSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.createASnapshotSMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createASnapshotSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateASnapshotSMetadata - errors', () => {
      it('should have a updateASnapshotSMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.updateASnapshotSMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateASnapshotSMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateASnapshotSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.updateASnapshotSMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateASnapshotSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showASnapshotSDetails - errors', () => {
      it('should have a showASnapshotSDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showASnapshotSDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showASnapshotSDetails(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showASnapshotSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.showASnapshotSDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showASnapshotSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateASnapshot - errors', () => {
      it('should have a updateASnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.updateASnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateASnapshot(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.updateASnapshot('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteASnapshot - errors', () => {
      it('should have a deleteASnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.deleteASnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteASnapshot(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteASnapshot('fakeparam', null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showASnapshotSMetadataForASpecificKey - errors', () => {
      it('should have a showASnapshotSMetadataForASpecificKey function', (done) => {
        try {
          assert.equal(true, typeof a.showASnapshotSMetadataForASpecificKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showASnapshotSMetadataForASpecificKey(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showASnapshotSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.showASnapshotSMetadataForASpecificKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showASnapshotSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.showASnapshotSMetadataForASpecificKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showASnapshotSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteASnapshotSMetadata - errors', () => {
      it('should have a deleteASnapshotSMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.deleteASnapshotSMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteASnapshotSMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteASnapshotSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteASnapshotSMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteASnapshotSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteASnapshotSMetadata('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteASnapshotSMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateASnapshotSMetadataForASpecificKey - errors', () => {
      it('should have a updateASnapshotSMetadataForASpecificKey function', (done) => {
        try {
          assert.equal(true, typeof a.updateASnapshotSMetadataForASpecificKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateASnapshotSMetadataForASpecificKey(null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateASnapshotSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.updateASnapshotSMetadataForASpecificKey('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateASnapshotSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.updateASnapshotSMetadataForASpecificKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateASnapshotSMetadataForASpecificKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetASnapshotSStatus - errors', () => {
      it('should have a resetASnapshotSStatus function', (done) => {
        try {
          assert.equal(true, typeof a.resetASnapshotSStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.resetASnapshotSStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-resetASnapshotSStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.resetASnapshotSStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-resetASnapshotSStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#manageAnExistingSnapshot - errors', () => {
      it('should have a manageAnExistingSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.manageAnExistingSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.manageAnExistingSnapshot(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-manageAnExistingSnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSummaryOfSnapshotsAvailableToManage - errors', () => {
      it('should have a listSummaryOfSnapshotsAvailableToManage function', (done) => {
        try {
          assert.equal(true, typeof a.listSummaryOfSnapshotsAvailableToManage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listSummaryOfSnapshotsAvailableToManage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listSummaryOfSnapshotsAvailableToManage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDetailOfSnapshotsAvailableToManage - errors', () => {
      it('should have a listDetailOfSnapshotsAvailableToManage function', (done) => {
        try {
          assert.equal(true, typeof a.listDetailOfSnapshotsAvailableToManage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listDetailOfSnapshotsAvailableToManage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listDetailOfSnapshotsAvailableToManage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#acceptAVolumeTransferNew - errors', () => {
      it('should have a acceptAVolumeTransferNew function', (done) => {
        try {
          assert.equal(true, typeof a.acceptAVolumeTransferNew === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.acceptAVolumeTransferNew(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-acceptAVolumeTransferNew', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transferId', (done) => {
        try {
          a.acceptAVolumeTransferNew('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'transferId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-acceptAVolumeTransferNew', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAVolumeTransferNew - errors', () => {
      it('should have a createAVolumeTransferNew function', (done) => {
        try {
          assert.equal(true, typeof a.createAVolumeTransferNew === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createAVolumeTransferNew(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAVolumeTransferNew', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVolumeTransfersForAProjectNew - errors', () => {
      it('should have a listVolumeTransfersForAProjectNew function', (done) => {
        try {
          assert.equal(true, typeof a.listVolumeTransfersForAProjectNew === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listVolumeTransfersForAProjectNew('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listVolumeTransfersForAProjectNew', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showVolumeTransferDetailNew - errors', () => {
      it('should have a showVolumeTransferDetailNew function', (done) => {
        try {
          assert.equal(true, typeof a.showVolumeTransferDetailNew === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showVolumeTransferDetailNew(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showVolumeTransferDetailNew', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transferId', (done) => {
        try {
          a.showVolumeTransferDetailNew('fakeparam', null, (data, error) => {
            try {
              const displayE = 'transferId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showVolumeTransferDetailNew', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAVolumeTransferNew - errors', () => {
      it('should have a deleteAVolumeTransferNew function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAVolumeTransferNew === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteAVolumeTransferNew(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolumeTransferNew', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transferId', (done) => {
        try {
          a.deleteAVolumeTransferNew('fakeparam', null, (data, error) => {
            try {
              const displayE = 'transferId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolumeTransferNew', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVolumeTransfersAndDetailsNew - errors', () => {
      it('should have a listVolumeTransfersAndDetailsNew function', (done) => {
        try {
          assert.equal(true, typeof a.listVolumeTransfersAndDetailsNew === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listVolumeTransfersAndDetailsNew('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listVolumeTransfersAndDetailsNew', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#acceptAVolumeTransfer - errors', () => {
      it('should have a acceptAVolumeTransfer function', (done) => {
        try {
          assert.equal(true, typeof a.acceptAVolumeTransfer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.acceptAVolumeTransfer(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-acceptAVolumeTransfer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transferId', (done) => {
        try {
          a.acceptAVolumeTransfer('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'transferId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-acceptAVolumeTransfer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAVolumeTransfer - errors', () => {
      it('should have a createAVolumeTransfer function', (done) => {
        try {
          assert.equal(true, typeof a.createAVolumeTransfer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createAVolumeTransfer(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAVolumeTransfer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVolumeTransfersForAProject - errors', () => {
      it('should have a listVolumeTransfersForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.listVolumeTransfersForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listVolumeTransfersForAProject('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listVolumeTransfersForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showVolumeTransferDetail - errors', () => {
      it('should have a showVolumeTransferDetail function', (done) => {
        try {
          assert.equal(true, typeof a.showVolumeTransferDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showVolumeTransferDetail(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showVolumeTransferDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transferId', (done) => {
        try {
          a.showVolumeTransferDetail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'transferId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showVolumeTransferDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAVolumeTransfer - errors', () => {
      it('should have a deleteAVolumeTransfer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAVolumeTransfer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteAVolumeTransfer(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolumeTransfer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transferId', (done) => {
        try {
          a.deleteAVolumeTransfer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'transferId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAVolumeTransfer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVolumeTransfersAndDetails - errors', () => {
      it('should have a listVolumeTransfersAndDetails function', (done) => {
        try {
          assert.equal(true, typeof a.listVolumeTransfersAndDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listVolumeTransfersAndDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listVolumeTransfersAndDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttachment - errors', () => {
      it('should have a deleteAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteAttachment(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.deleteAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAttachmentDetails - errors', () => {
      it('should have a showAttachmentDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showAttachmentDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAttachmentDetails(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAttachmentDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.showAttachmentDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAttachmentDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnAttachment - errors', () => {
      it('should have a updateAnAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateAnAttachment(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAnAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.updateAnAttachment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAnAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttachmentsWithDetails - errors', () => {
      it('should have a listAttachmentsWithDetails function', (done) => {
        try {
          assert.equal(true, typeof a.listAttachmentsWithDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listAttachmentsWithDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAttachmentsWithDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttachments - errors', () => {
      it('should have a listAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.listAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listAttachments('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAttachment - errors', () => {
      it('should have a createAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createAttachment(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#completeAttachment - errors', () => {
      it('should have a completeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.completeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.completeAttachment(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-completeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.completeAttachment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-completeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllBackEndStoragePools - errors', () => {
      it('should have a listAllBackEndStoragePools function', (done) => {
        try {
          assert.equal(true, typeof a.listAllBackEndStoragePools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listAllBackEndStoragePools('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAllBackEndStoragePools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBackupsWithDetail - errors', () => {
      it('should have a listBackupsWithDetail function', (done) => {
        try {
          assert.equal(true, typeof a.listBackupsWithDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listBackupsWithDetail('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listBackupsWithDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showBackupDetail - errors', () => {
      it('should have a showBackupDetail function', (done) => {
        try {
          assert.equal(true, typeof a.showBackupDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showBackupDetail(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showBackupDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing backupId', (done) => {
        try {
          a.showBackupDetail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'backupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showBackupDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteABackup - errors', () => {
      it('should have a deleteABackup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteABackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteABackup(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing backupId', (done) => {
        try {
          a.deleteABackup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'backupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateABackup - errors', () => {
      it('should have a updateABackup function', (done) => {
        try {
          assert.equal(true, typeof a.updateABackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateABackup(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing backupId', (done) => {
        try {
          a.updateABackup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'backupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreABackup - errors', () => {
      it('should have a restoreABackup function', (done) => {
        try {
          assert.equal(true, typeof a.restoreABackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.restoreABackup(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-restoreABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing backupId', (done) => {
        try {
          a.restoreABackup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'backupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-restoreABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createABackup - errors', () => {
      it('should have a createABackup function', (done) => {
        try {
          assert.equal(true, typeof a.createABackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createABackup(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBackupsForProject - errors', () => {
      it('should have a listBackupsForProject function', (done) => {
        try {
          assert.equal(true, typeof a.listBackupsForProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listBackupsForProject('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listBackupsForProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportABackup - errors', () => {
      it('should have a exportABackup function', (done) => {
        try {
          assert.equal(true, typeof a.exportABackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.exportABackup(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-exportABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing backupId', (done) => {
        try {
          a.exportABackup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'backupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-exportABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importABackup - errors', () => {
      it('should have a importABackup function', (done) => {
        try {
          assert.equal(true, typeof a.importABackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.importABackup(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-importABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forceDeleteABackup - errors', () => {
      it('should have a forceDeleteABackup function', (done) => {
        try {
          assert.equal(true, typeof a.forceDeleteABackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.forceDeleteABackup(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-forceDeleteABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing backupId', (done) => {
        try {
          a.forceDeleteABackup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'backupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-forceDeleteABackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAllBackEndCapabilities - errors', () => {
      it('should have a showAllBackEndCapabilities function', (done) => {
        try {
          assert.equal(true, typeof a.showAllBackEndCapabilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAllBackEndCapabilities(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAllBackEndCapabilities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostname', (done) => {
        try {
          a.showAllBackEndCapabilities('fakeparam', null, (data, error) => {
            try {
              const displayE = 'hostname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAllBackEndCapabilities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listProjectSConsistencyGroups - errors', () => {
      it('should have a listProjectSConsistencyGroups function', (done) => {
        try {
          assert.equal(true, typeof a.listProjectSConsistencyGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listProjectSConsistencyGroups('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listProjectSConsistencyGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAConsistencyGroup - errors', () => {
      it('should have a createAConsistencyGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createAConsistencyGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createAConsistencyGroup(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAConsistencyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAConsistencyGroupSDetails - errors', () => {
      it('should have a showAConsistencyGroupSDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showAConsistencyGroupSDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAConsistencyGroupSDetails(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAConsistencyGroupSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing consistencygroupId', (done) => {
        try {
          a.showAConsistencyGroupSDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'consistencygroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAConsistencyGroupSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAConsistencyGroupFromSource - errors', () => {
      it('should have a createAConsistencyGroupFromSource function', (done) => {
        try {
          assert.equal(true, typeof a.createAConsistencyGroupFromSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createAConsistencyGroupFromSource(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAConsistencyGroupFromSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAConsistencyGroup - errors', () => {
      it('should have a deleteAConsistencyGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAConsistencyGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteAConsistencyGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAConsistencyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing consistencygroupId', (done) => {
        try {
          a.deleteAConsistencyGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'consistencygroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAConsistencyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listConsistencyGroupsAndDetails - errors', () => {
      it('should have a listConsistencyGroupsAndDetails function', (done) => {
        try {
          assert.equal(true, typeof a.listConsistencyGroupsAndDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listConsistencyGroupsAndDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listConsistencyGroupsAndDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAConsistencyGroup - errors', () => {
      it('should have a updateAConsistencyGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateAConsistencyGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateAConsistencyGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAConsistencyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing consistencygroupId', (done) => {
        try {
          a.updateAConsistencyGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'consistencygroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateAConsistencyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAConsistencyGroupSnapshot - errors', () => {
      it('should have a deleteAConsistencyGroupSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAConsistencyGroupSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteAConsistencyGroupSnapshot(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAConsistencyGroupSnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cgsnapshotId', (done) => {
        try {
          a.deleteAConsistencyGroupSnapshot('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cgsnapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAConsistencyGroupSnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showConsistencyGroupSnapshotDetail - errors', () => {
      it('should have a showConsistencyGroupSnapshotDetail function', (done) => {
        try {
          assert.equal(true, typeof a.showConsistencyGroupSnapshotDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showConsistencyGroupSnapshotDetail(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showConsistencyGroupSnapshotDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cgsnapshotId', (done) => {
        try {
          a.showConsistencyGroupSnapshotDetail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cgsnapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showConsistencyGroupSnapshotDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllConsistencyGroupSnapshotsWithDetails - errors', () => {
      it('should have a listAllConsistencyGroupSnapshotsWithDetails function', (done) => {
        try {
          assert.equal(true, typeof a.listAllConsistencyGroupSnapshotsWithDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listAllConsistencyGroupSnapshotsWithDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAllConsistencyGroupSnapshotsWithDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllConsistencyGroupSnapshots - errors', () => {
      it('should have a listAllConsistencyGroupSnapshots function', (done) => {
        try {
          assert.equal(true, typeof a.listAllConsistencyGroupSnapshots === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listAllConsistencyGroupSnapshots('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAllConsistencyGroupSnapshots', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAConsistencyGroupSnapshot - errors', () => {
      it('should have a createAConsistencyGroupSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.createAConsistencyGroupSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createAConsistencyGroupSnapshot(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAConsistencyGroupSnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllCinderServices - errors', () => {
      it('should have a listAllCinderServices function', (done) => {
        try {
          assert.equal(true, typeof a.listAllCinderServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listAllCinderServices('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAllCinderServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableACinderService - errors', () => {
      it('should have a disableACinderService function', (done) => {
        try {
          assert.equal(true, typeof a.disableACinderService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.disableACinderService(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-disableACinderService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logDisabledCinderServiceInformation - errors', () => {
      it('should have a logDisabledCinderServiceInformation function', (done) => {
        try {
          assert.equal(true, typeof a.logDisabledCinderServiceInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.logDisabledCinderServiceInformation(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-logDisabledCinderServiceInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableACinderService - errors', () => {
      it('should have a enableACinderService function', (done) => {
        try {
          assert.equal(true, typeof a.enableACinderService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.enableACinderService(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-enableACinderService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentLogLevelsForCinderServices - errors', () => {
      it('should have a getCurrentLogLevelsForCinderServices function', (done) => {
        try {
          assert.equal(true, typeof a.getCurrentLogLevelsForCinderServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getCurrentLogLevelsForCinderServices(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-getCurrentLogLevelsForCinderServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setLogLevelsOfCinderServicesDynamically - errors', () => {
      it('should have a setLogLevelsOfCinderServicesDynamically function', (done) => {
        try {
          assert.equal(true, typeof a.setLogLevelsOfCinderServicesDynamically === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.setLogLevelsOfCinderServicesDynamically(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-setLogLevelsOfCinderServicesDynamically', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#freezeACinderBackendHost - errors', () => {
      it('should have a freezeACinderBackendHost function', (done) => {
        try {
          assert.equal(true, typeof a.freezeACinderBackendHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.freezeACinderBackendHost(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-freezeACinderBackendHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#thawACinderBackendHost - errors', () => {
      it('should have a thawACinderBackendHost function', (done) => {
        try {
          assert.equal(true, typeof a.thawACinderBackendHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.thawACinderBackendHost(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-thawACinderBackendHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#failoverACinderBackendHost - errors', () => {
      it('should have a failoverACinderBackendHost function', (done) => {
        try {
          assert.equal(true, typeof a.failoverACinderBackendHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.failoverACinderBackendHost(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-failoverACinderBackendHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroups - errors', () => {
      it('should have a listGroups function', (done) => {
        try {
          assert.equal(true, typeof a.listGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listGroups('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGroup - errors', () => {
      it('should have a createGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createGroup(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showGroupDetails - errors', () => {
      it('should have a showGroupDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showGroupDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showGroupDetails(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showGroupDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.showGroupDetails('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showGroupDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGroup - errors', () => {
      it('should have a updateGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.updateGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGroupFromSource - errors', () => {
      it('should have a createGroupFromSource function', (done) => {
        try {
          assert.equal(true, typeof a.createGroupFromSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createGroupFromSource(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createGroupFromSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroup - errors', () => {
      it('should have a deleteGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.deleteGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupsWithDetails - errors', () => {
      it('should have a listGroupsWithDetails function', (done) => {
        try {
          assert.equal(true, typeof a.listGroupsWithDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listGroupsWithDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listGroupsWithDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroupSnapshot - errors', () => {
      it('should have a deleteGroupSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGroupSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteGroupSnapshot(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteGroupSnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupSnapshotId', (done) => {
        try {
          a.deleteGroupSnapshot('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupSnapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteGroupSnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showGroupSnapshotDetails - errors', () => {
      it('should have a showGroupSnapshotDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showGroupSnapshotDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showGroupSnapshotDetails(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showGroupSnapshotDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupSnapshotId', (done) => {
        try {
          a.showGroupSnapshotDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupSnapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showGroupSnapshotDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupSnapshotsWithDetails - errors', () => {
      it('should have a listGroupSnapshotsWithDetails function', (done) => {
        try {
          assert.equal(true, typeof a.listGroupSnapshotsWithDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listGroupSnapshotsWithDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listGroupSnapshotsWithDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupSnapshots - errors', () => {
      it('should have a listGroupSnapshots function', (done) => {
        try {
          assert.equal(true, typeof a.listGroupSnapshots === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listGroupSnapshots('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listGroupSnapshots', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGroupSnapshot - errors', () => {
      it('should have a createGroupSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.createGroupSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createGroupSnapshot(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createGroupSnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetGroupSnapshotStatus - errors', () => {
      it('should have a resetGroupSnapshotStatus function', (done) => {
        try {
          assert.equal(true, typeof a.resetGroupSnapshotStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.resetGroupSnapshotStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-resetGroupSnapshotStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupSnapshotId', (done) => {
        try {
          a.resetGroupSnapshotStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupSnapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-resetGroupSnapshotStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGroupType - errors', () => {
      it('should have a updateGroupType function', (done) => {
        try {
          assert.equal(true, typeof a.updateGroupType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateGroupType(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupTypeId', (done) => {
        try {
          a.updateGroupType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showGroupTypeDetails - errors', () => {
      it('should have a showGroupTypeDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showGroupTypeDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showGroupTypeDetails(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showGroupTypeDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupTypeId', (done) => {
        try {
          a.showGroupTypeDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showGroupTypeDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroupType - errors', () => {
      it('should have a deleteGroupType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGroupType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupTypeId', (done) => {
        try {
          a.deleteGroupType(null, null, (data, error) => {
            try {
              const displayE = 'groupTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteGroupType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showDefaultGroupTypeDetails - errors', () => {
      it('should have a showDefaultGroupTypeDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showDefaultGroupTypeDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showDefaultGroupTypeDetails(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showDefaultGroupTypeDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupTypes - errors', () => {
      it('should have a listGroupTypes function', (done) => {
        try {
          assert.equal(true, typeof a.listGroupTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listGroupTypes('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listGroupTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGroupType - errors', () => {
      it('should have a createGroupType function', (done) => {
        try {
          assert.equal(true, typeof a.createGroupType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createGroupType(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrUpdateGroupSpecsForAGroupType - errors', () => {
      it('should have a createOrUpdateGroupSpecsForAGroupType function', (done) => {
        try {
          assert.equal(true, typeof a.createOrUpdateGroupSpecsForAGroupType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createOrUpdateGroupSpecsForAGroupType(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createOrUpdateGroupSpecsForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupTypeId', (done) => {
        try {
          a.createOrUpdateGroupSpecsForAGroupType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createOrUpdateGroupSpecsForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupSpecsForAGroupType - errors', () => {
      it('should have a listGroupSpecsForAGroupType function', (done) => {
        try {
          assert.equal(true, typeof a.listGroupSpecsForAGroupType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listGroupSpecsForAGroupType(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listGroupSpecsForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupTypeId', (done) => {
        try {
          a.listGroupSpecsForAGroupType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listGroupSpecsForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showOneSpecificGroupSpecForAGroupType - errors', () => {
      it('should have a showOneSpecificGroupSpecForAGroupType function', (done) => {
        try {
          assert.equal(true, typeof a.showOneSpecificGroupSpecForAGroupType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showOneSpecificGroupSpecForAGroupType(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showOneSpecificGroupSpecForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupTypeId', (done) => {
        try {
          a.showOneSpecificGroupSpecForAGroupType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showOneSpecificGroupSpecForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing specId', (done) => {
        try {
          a.showOneSpecificGroupSpecForAGroupType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'specId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showOneSpecificGroupSpecForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOneSpecificGroupSpecForAGroupType - errors', () => {
      it('should have a updateOneSpecificGroupSpecForAGroupType function', (done) => {
        try {
          assert.equal(true, typeof a.updateOneSpecificGroupSpecForAGroupType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateOneSpecificGroupSpecForAGroupType(null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateOneSpecificGroupSpecForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupTypeId', (done) => {
        try {
          a.updateOneSpecificGroupSpecForAGroupType('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'groupTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateOneSpecificGroupSpecForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing specId', (done) => {
        try {
          a.updateOneSpecificGroupSpecForAGroupType('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'specId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateOneSpecificGroupSpecForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOneSpecificGroupSpecForAGroupType - errors', () => {
      it('should have a deleteOneSpecificGroupSpecForAGroupType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOneSpecificGroupSpecForAGroupType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteOneSpecificGroupSpecForAGroupType(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteOneSpecificGroupSpecForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupTypeId', (done) => {
        try {
          a.deleteOneSpecificGroupSpecForAGroupType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteOneSpecificGroupSpecForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing specId', (done) => {
        try {
          a.deleteOneSpecificGroupSpecForAGroupType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'specId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteOneSpecificGroupSpecForAGroupType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllHostsForAProject - errors', () => {
      it('should have a listAllHostsForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.listAllHostsForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminProjectId', (done) => {
        try {
          a.listAllHostsForAProject(null, (data, error) => {
            try {
              const displayE = 'adminProjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listAllHostsForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showHostDetailsForAProject - errors', () => {
      it('should have a showHostDetailsForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.showHostDetailsForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminProjectId', (done) => {
        try {
          a.showHostDetailsForAProject(null, null, (data, error) => {
            try {
              const displayE = 'adminProjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showHostDetailsForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostName', (done) => {
        try {
          a.showHostDetailsForAProject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showHostDetailsForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAbsoluteLimitsForProject - errors', () => {
      it('should have a showAbsoluteLimitsForProject function', (done) => {
        try {
          assert.equal(true, typeof a.showAbsoluteLimitsForProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAbsoluteLimitsForProject(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAbsoluteLimitsForProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMessage - errors', () => {
      it('should have a deleteMessage function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMessage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteMessage(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteMessage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing messageId', (done) => {
        try {
          a.deleteMessage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'messageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteMessage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showMessageDetails - errors', () => {
      it('should have a showMessageDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showMessageDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showMessageDetails(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showMessageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing messageId', (done) => {
        try {
          a.showMessageDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'messageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showMessageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listMessages - errors', () => {
      it('should have a listMessages function', (done) => {
        try {
          assert.equal(true, typeof a.listMessages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listMessages('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listMessages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listResourceFilters - errors', () => {
      it('should have a listResourceFilters function', (done) => {
        try {
          assert.equal(true, typeof a.listResourceFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listResourceFilters('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listResourceFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateAQoSSpecificationFromAllAssociations - errors', () => {
      it('should have a disassociateAQoSSpecificationFromAllAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.disassociateAQoSSpecificationFromAllAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.disassociateAQoSSpecificationFromAllAssociations(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-disassociateAQoSSpecificationFromAllAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosId', (done) => {
        try {
          a.disassociateAQoSSpecificationFromAllAssociations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'qosId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-disassociateAQoSSpecificationFromAllAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unsetKeysInAQoSSpecification - errors', () => {
      it('should have a unsetKeysInAQoSSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.unsetKeysInAQoSSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.unsetKeysInAQoSSpecification(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-unsetKeysInAQoSSpecification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosId', (done) => {
        try {
          a.unsetKeysInAQoSSpecification('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'qosId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-unsetKeysInAQoSSpecification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAssociationsForAQoSSpecification - errors', () => {
      it('should have a getAllAssociationsForAQoSSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAssociationsForAQoSSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getAllAssociationsForAQoSSpecification(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-getAllAssociationsForAQoSSpecification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosId', (done) => {
        try {
          a.getAllAssociationsForAQoSSpecification('fakeparam', null, (data, error) => {
            try {
              const displayE = 'qosId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-getAllAssociationsForAQoSSpecification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateQoSSpecificationWithAVolumeType - errors', () => {
      it('should have a associateQoSSpecificationWithAVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.associateQoSSpecificationWithAVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volTypeId', (done) => {
        try {
          a.associateQoSSpecificationWithAVolumeType(null, null, null, (data, error) => {
            try {
              const displayE = 'volTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-associateQoSSpecificationWithAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.associateQoSSpecificationWithAVolumeType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-associateQoSSpecificationWithAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosId', (done) => {
        try {
          a.associateQoSSpecificationWithAVolumeType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'qosId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-associateQoSSpecificationWithAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateQoSSpecificationFromAVolumeType - errors', () => {
      it('should have a disassociateQoSSpecificationFromAVolumeType function', (done) => {
        try {
          assert.equal(true, typeof a.disassociateQoSSpecificationFromAVolumeType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volTypeId', (done) => {
        try {
          a.disassociateQoSSpecificationFromAVolumeType(null, null, null, (data, error) => {
            try {
              const displayE = 'volTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-disassociateQoSSpecificationFromAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.disassociateQoSSpecificationFromAVolumeType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-disassociateQoSSpecificationFromAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosId', (done) => {
        try {
          a.disassociateQoSSpecificationFromAVolumeType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'qosId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-disassociateQoSSpecificationFromAVolumeType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAQoSSpecificationDetails - errors', () => {
      it('should have a showAQoSSpecificationDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showAQoSSpecificationDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAQoSSpecificationDetails(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAQoSSpecificationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosId', (done) => {
        try {
          a.showAQoSSpecificationDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'qosId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showAQoSSpecificationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setKeysInAQoSSpecification - errors', () => {
      it('should have a setKeysInAQoSSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.setKeysInAQoSSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.setKeysInAQoSSpecification(null, null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-setKeysInAQoSSpecification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosId', (done) => {
        try {
          a.setKeysInAQoSSpecification('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'qosId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-setKeysInAQoSSpecification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAQoSSpecification - errors', () => {
      it('should have a deleteAQoSSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAQoSSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteAQoSSpecification('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAQoSSpecification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosId', (done) => {
        try {
          a.deleteAQoSSpecification('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'qosId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteAQoSSpecification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAQoSSpecification - errors', () => {
      it('should have a createAQoSSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.createAQoSSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.createAQoSSpecification(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-createAQoSSpecification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listQoSSpecifications - errors', () => {
      it('should have a listQoSSpecifications function', (done) => {
        try {
          assert.equal(true, typeof a.listQoSSpecifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listQoSSpecifications('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-listQoSSpecifications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showQuotaClassesForAProject - errors', () => {
      it('should have a showQuotaClassesForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.showQuotaClassesForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing quotaClassName', (done) => {
        try {
          a.showQuotaClassesForAProject(null, null, (data, error) => {
            try {
              const displayE = 'quotaClassName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showQuotaClassesForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminProjectId', (done) => {
        try {
          a.showQuotaClassesForAProject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'adminProjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showQuotaClassesForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateQuotaClassesForAProject - errors', () => {
      it('should have a updateQuotaClassesForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.updateQuotaClassesForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminProjectId', (done) => {
        try {
          a.updateQuotaClassesForAProject(null, null, null, (data, error) => {
            try {
              const displayE = 'adminProjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateQuotaClassesForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing quotaClassName', (done) => {
        try {
          a.updateQuotaClassesForAProject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'quotaClassName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateQuotaClassesForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showQuotasForAProject - errors', () => {
      it('should have a showQuotasForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.showQuotasForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminProjectId', (done) => {
        try {
          a.showQuotasForAProject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'adminProjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showQuotasForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showQuotasForAProject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-showQuotasForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateQuotasForAProject - errors', () => {
      it('should have a updateQuotasForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.updateQuotasForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminProjectId', (done) => {
        try {
          a.updateQuotasForAProject(null, null, null, (data, error) => {
            try {
              const displayE = 'adminProjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateQuotasForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateQuotasForAProject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-updateQuotasForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteQuotasForAProject - errors', () => {
      it('should have a deleteQuotasForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteQuotasForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteQuotasForAProject(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteQuotasForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminProjectId', (done) => {
        try {
          a.deleteQuotasForAProject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'adminProjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-deleteQuotasForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultQuotasForAProject - errors', () => {
      it('should have a getDefaultQuotasForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultQuotasForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getDefaultQuotasForAProject(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-getDefaultQuotasForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminProjectId', (done) => {
        try {
          a.getDefaultQuotasForAProject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'adminProjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-getDefaultQuotasForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cleanupServices - errors', () => {
      it('should have a cleanupServices function', (done) => {
        try {
          assert.equal(true, typeof a.cleanupServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.cleanupServices(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_cinder-adapter-cleanupServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
