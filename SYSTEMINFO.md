# OpenStack Cinder

Vendor: OpenStack
Homepage: https://www.openstack.org/

Product: Cinder
Product Page: https://www.openstack.org/software/releases/ocata/components/cinder

## Introduction
We classify OpenStack Cinder into the Cloud domain as OpenStack Cinder provides a solution for managing block storage, allowing cloud environments to handle persistent data storage dynamically.

## Why Integrate
The OpenStack Cinder adapter from Itential is used to integrate the Itential Automation Platform (IAP) with OpenStack Cinder. With this adapter you have the ability to perform operations on items such as:

- Hosts
- Resources
- Services

## Additional Product Documentation
[OpenStack Cinder API Documentation](https://docs.openstack.org/api-ref/block-storage/v3/index.html)