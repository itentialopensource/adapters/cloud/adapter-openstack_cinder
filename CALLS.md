## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for OpenStack Cinder. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for OpenStack Cinder.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Openstack_cinder. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listAllApiVersions(callback)</td>
    <td style="padding:15px">List All Api Versions</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAPIV3Details(callback)</td>
    <td style="padding:15px">Show API v3 details</td>
    <td style="padding:15px">{base_path}/{version}/v3/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listKnownAPIExtensions(projectId, callback)</td>
    <td style="padding:15px">List Known API extensions</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/extensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAVolumeType(projectId, volumeTypeId, body, callback)</td>
    <td style="padding:15px">Update a volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVolumeTypeDetail(projectId, volumeTypeId, callback)</td>
    <td style="padding:15px">Show volume type detail</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAVolumeType(projectId, volumeTypeId, callback)</td>
    <td style="padding:15px">Delete a volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExtraSpecsForVolumeType(projectId, volumeTypeId, body, callback)</td>
    <td style="padding:15px">Create or update extra specs for volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/extra_specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAllExtraSpecificationsForVolumeType(projectId, volumeTypeId, callback)</td>
    <td style="padding:15px">Show all extra specifications for volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/extra_specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showExtraSpecificationForVolumeType(projectId, volumeTypeId, key, callback)</td>
    <td style="padding:15px">Show extra specification for volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/extra_specs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExtraSpecificationForVolumeType(projectId, volumeTypeId, key, body, callback)</td>
    <td style="padding:15px">Update extra specification for volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/extra_specs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtraSpecificationForVolumeType(projectId, volumeTypeId, key, callback)</td>
    <td style="padding:15px">Delete extra specification for volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/extra_specs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDefaultVolumeType(projectId, callback)</td>
    <td style="padding:15px">Show default volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllVolumeTypes(isPublic, sort, limit, offset, marker, projectId, callback)</td>
    <td style="padding:15px">List all volume types</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAVolumeType(projectId, body, callback)</td>
    <td style="padding:15px">Create a volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAnEncryptionType(projectId, volumeTypeId, callback)</td>
    <td style="padding:15px">Show an encryption type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/encryption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAnEncryptionType(projectId, volumeTypeId, body, callback)</td>
    <td style="padding:15px">Create an encryption type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/encryption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showEncryptionSpecsItem(projectId, volumeTypeId, key, callback)</td>
    <td style="padding:15px">Show encryption specs item</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/encryption/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnEncryptionType(projectId, volumeTypeId, encryptionId, callback)</td>
    <td style="padding:15px">Delete an encryption type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/encryption/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnEncryptionType(projectId, volumeTypeId, encryptionId, body, callback)</td>
    <td style="padding:15px">Update an encryption type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/encryption/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPrivateVolumeTypeAccessToProject(projectId, volumeType, body, callback)</td>
    <td style="padding:15px">Add private volume type access to project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPrivateVolumeTypeAccessDetail(projectId, volumeType, callback)</td>
    <td style="padding:15px">List private volume type access detail</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/types/{pathv2}/os-volume-type-access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateADefaultVolumeType(projectId, body, callback)</td>
    <td style="padding:15px">Create or update a default volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/default-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showADefaultVolumeType(projectId, callback)</td>
    <td style="padding:15px">Show a default volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/default-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteADefaultVolumeType(projectId, callback)</td>
    <td style="padding:15px">Delete a default volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/default-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDefaultVolumeTypes(callback)</td>
    <td style="padding:15px">List default volume types</td>
    <td style="padding:15px">{base_path}/{version}/v3/default-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccessibleVolumesWithDetails(allTenants, sort, limit, offset, marker, withCount, createdAt, updatedAt, consumesQuota, projectId, callback)</td>
    <td style="padding:15px">List accessible volumes with details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAVolume(projectId, body, callback)</td>
    <td style="padding:15px">Create a volume</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccessibleVolumes(allTenants, sort, limit, offset, marker, withCount, createdAt, consumesQuota, updatedAt, projectId, callback)</td>
    <td style="padding:15px">List accessible volumes</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAVolumeSDetails(projectId, volumeId, callback)</td>
    <td style="padding:15px">Show a volume’s details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAVolume(projectId, volumeId, body, callback)</td>
    <td style="padding:15px">Update a volume</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAVolume(cascade, force, projectId, volumeId, callback)</td>
    <td style="padding:15px">Delete a volume</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMetadataForVolume(projectId, volumeId, body, callback)</td>
    <td style="padding:15px">Create metadata for volume</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAVolumeSMetadata(projectId, volumeId, callback)</td>
    <td style="padding:15px">Show a volume’s metadata</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAVolumeSMetadata(projectId, volumeId, body, callback)</td>
    <td style="padding:15px">Update a volume’s metadata</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAVolumeSMetadataForASpecificKey(projectId, volumeId, key, callback)</td>
    <td style="padding:15px">Show a volume’s metadata for a specific key</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAVolumeSMetadata(projectId, volumeId, key, callback)</td>
    <td style="padding:15px">Delete a volume’s metadata</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAVolumeSMetadataForASpecificKey(projectId, volumeId, key, body, callback)</td>
    <td style="padding:15px">Update a volume’s metadata for a specific key</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVolumesSummary(allTenants, projectId, callback)</td>
    <td style="padding:15px">Get volumes summary</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">extendAVolumeSize(projectId, volumeId, body, callback)</td>
    <td style="padding:15px">Extend a volume size</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volumes/{pathv2}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageAnExistingVolume(projectId, body, callback)</td>
    <td style="padding:15px">Manage an existing volume</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/manageable_volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSummaryOfVolumesAvailableToManage(sort, offset, limit, marker, projectId, host, callback)</td>
    <td style="padding:15px">List summary of volumes available to manage</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/manageable_volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDetailOfVolumesAvailableToManage(sort, offset, limit, marker, host, projectId, callback)</td>
    <td style="padding:15px">List detail of volumes available to manage</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/manageable_volumes/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSnapshotsAndDetails(allTenants, sort, limit, offset, marker, withCount, consumesQuota, projectId, callback)</td>
    <td style="padding:15px">List snapshots and details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createASnapshot(projectId, body, callback)</td>
    <td style="padding:15px">Create a snapshot</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccessibleSnapshots(allTenants, sort, limit, offset, marker, consumesQuota, withCount, projectId, callback)</td>
    <td style="padding:15px">List accessible snapshots</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showASnapshotSMetadata(projectId, snapshotId, callback)</td>
    <td style="padding:15px">Show a snapshot’s metadata</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createASnapshotSMetadata(projectId, snapshotId, body, callback)</td>
    <td style="padding:15px">Create a snapshot’s metadata</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateASnapshotSMetadata(projectId, snapshotId, body, callback)</td>
    <td style="padding:15px">Update a snapshot’s metadata</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showASnapshotSDetails(projectId, snapshotId, callback)</td>
    <td style="padding:15px">Show a snapshot’s details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateASnapshot(projectId, snapshotId, body, callback)</td>
    <td style="padding:15px">Update a snapshot</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteASnapshot(projectId, snapshotId, callback)</td>
    <td style="padding:15px">Delete a snapshot</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showASnapshotSMetadataForASpecificKey(projectId, snapshotId, key, callback)</td>
    <td style="padding:15px">Show a snapshot’s metadata for a specific key</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshot/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteASnapshotSMetadata(projectId, snapshotId, key, callback)</td>
    <td style="padding:15px">Delete a snapshot’s metadata</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateASnapshotSMetadataForASpecificKey(projectId, snapshotId, key, body, callback)</td>
    <td style="padding:15px">Update a snapshot’s metadata for a specific key</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetASnapshotSStatus(projectId, snapshotId, body, callback)</td>
    <td style="padding:15px">Reset a snapshot’s status</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/snapshots/{pathv2}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageAnExistingSnapshot(projectId, body, callback)</td>
    <td style="padding:15px">Manage an existing snapshot</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/manageable_snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSummaryOfSnapshotsAvailableToManage(sort, offset, limit, marker, host, projectId, callback)</td>
    <td style="padding:15px">List summary of snapshots available to manage</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/manageable_snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDetailOfSnapshotsAvailableToManage(sort, offset, limit, marker, host, projectId, callback)</td>
    <td style="padding:15px">List detail of snapshots available to manage</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/manageable_snapshots/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acceptAVolumeTransferNew(projectId, transferId, body, callback)</td>
    <td style="padding:15px">Accept a volume transfer</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-volume-transfer/{pathv2}/accept?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAVolumeTransferNew(projectId, body, callback)</td>
    <td style="padding:15px">Create a volume transfer</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-volume-transfer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVolumeTransfersForAProjectNew(allTenants, projectId, callback)</td>
    <td style="padding:15px">List volume transfers for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-volume-transfer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVolumeTransferDetailNew(projectId, transferId, callback)</td>
    <td style="padding:15px">Show volume transfer detail</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-volume-transfer/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAVolumeTransferNew(projectId, transferId, callback)</td>
    <td style="padding:15px">Delete a volume transfer</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-volume-transfer/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVolumeTransfersAndDetailsNew(allTenants, projectId, callback)</td>
    <td style="padding:15px">List volume transfers and details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-volume-transfer/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acceptAVolumeTransfer(projectId, transferId, body, callback)</td>
    <td style="padding:15px">Accept a volume transfer</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volume-transfers/{pathv2}/accept?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAVolumeTransfer(projectId, body, callback)</td>
    <td style="padding:15px">Create a volume transfer</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volume-transfers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVolumeTransfersForAProject(allTenants, limit, offset, marker, sortKey, sortDir, projectId, callback)</td>
    <td style="padding:15px">List volume transfers for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volume-transfers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVolumeTransferDetail(projectId, transferId, callback)</td>
    <td style="padding:15px">Show volume transfer detail</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volume-transfers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAVolumeTransfer(projectId, transferId, callback)</td>
    <td style="padding:15px">Delete a volume transfer</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volume-transfers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVolumeTransfersAndDetails(allTenants, projectId, callback)</td>
    <td style="padding:15px">List volume transfers and details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/volume-transfers/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAttachment(projectId, attachmentId, callback)</td>
    <td style="padding:15px">Delete attachment</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/attachments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAttachmentDetails(projectId, attachmentId, callback)</td>
    <td style="padding:15px">Show attachment details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/attachments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnAttachment(projectId, attachmentId, body, callback)</td>
    <td style="padding:15px">Update an attachment</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/attachments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttachmentsWithDetails(allTenants, sort, limit, offset, marker, projectId, callback)</td>
    <td style="padding:15px">List attachments with details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/attachments/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttachments(allTenants, sort, limit, offset, marker, projectId, callback)</td>
    <td style="padding:15px">List attachments</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAttachment(projectId, body, callback)</td>
    <td style="padding:15px">Create attachment</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">completeAttachment(projectId, attachmentId, body, callback)</td>
    <td style="padding:15px">Complete attachment</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/attachments/{pathv2}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllBackEndStoragePools(detail, projectId, callback)</td>
    <td style="padding:15px">List all back-end storage pools</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/scheduler-stats/get_pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBackupsWithDetail(allTenants, sort, limit, offset, marker, withCount, projectId, callback)</td>
    <td style="padding:15px">List backups with detail</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/backups/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showBackupDetail(projectId, backupId, callback)</td>
    <td style="padding:15px">Show backup detail</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/backups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteABackup(projectId, backupId, callback)</td>
    <td style="padding:15px">Delete a backup</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/backups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateABackup(projectId, backupId, body, callback)</td>
    <td style="padding:15px">Update a backup</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/backups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreABackup(projectId, backupId, body, callback)</td>
    <td style="padding:15px">Restore a backup</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/backups/{pathv2}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createABackup(projectId, body, callback)</td>
    <td style="padding:15px">Create a backup</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBackupsForProject(allTenants, sort, limit, marker, withCount, projectId, callback)</td>
    <td style="padding:15px">List backups for project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportABackup(projectId, backupId, callback)</td>
    <td style="padding:15px">Export a backup</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/backups/{pathv2}/export_record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importABackup(projectId, body, callback)</td>
    <td style="padding:15px">Import a backup</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/backups/import_record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceDeleteABackup(projectId, backupId, body, callback)</td>
    <td style="padding:15px">Force-delete a backup</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/backups/{pathv2}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAllBackEndCapabilities(projectId, hostname, callback)</td>
    <td style="padding:15px">Show all back-end capabilities</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/capabilities/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProjectSConsistencyGroups(allTenants, sort, limit, marker, projectId, callback)</td>
    <td style="padding:15px">List project’s consistency groups</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/consistencygroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAConsistencyGroup(projectId, body, callback)</td>
    <td style="padding:15px">Create a consistency group</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/consistencygroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAConsistencyGroupSDetails(projectId, consistencygroupId, callback)</td>
    <td style="padding:15px">Show a consistency group’s details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/consistencygroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAConsistencyGroupFromSource(projectId, body, callback)</td>
    <td style="padding:15px">Create a consistency group from source</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/consistencygroups/create_from_src?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAConsistencyGroup(projectId, consistencygroupId, body, callback)</td>
    <td style="padding:15px">Delete a consistency group</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/consistencygroups/{pathv2}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listConsistencyGroupsAndDetails(allTenants, sort, limit, marker, projectId, callback)</td>
    <td style="padding:15px">List consistency groups and details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/consistencygroups/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAConsistencyGroup(projectId, consistencygroupId, body, callback)</td>
    <td style="padding:15px">Update a consistency group</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/consistencygroups/{pathv2}/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAConsistencyGroupSnapshot(projectId, cgsnapshotId, callback)</td>
    <td style="padding:15px">Delete a consistency group snapshot</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/cgsnapshots/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showConsistencyGroupSnapshotDetail(projectId, cgsnapshotId, callback)</td>
    <td style="padding:15px">Show consistency group snapshot detail</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/cgsnapshots/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllConsistencyGroupSnapshotsWithDetails(allTenants, projectId, callback)</td>
    <td style="padding:15px">List all consistency group snapshots with details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/cgsnapshots/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllConsistencyGroupSnapshots(allTenants, projectId, callback)</td>
    <td style="padding:15px">List all consistency group snapshots</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/cgsnapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAConsistencyGroupSnapshot(projectId, body, callback)</td>
    <td style="padding:15px">Create a consistency group snapshot</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/cgsnapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllCinderServices(host, binary, projectId, callback)</td>
    <td style="padding:15px">List All Cinder Services</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableACinderService(projectId, body, callback)</td>
    <td style="padding:15px">Disable a Cinder Service</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-services/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logDisabledCinderServiceInformation(projectId, body, callback)</td>
    <td style="padding:15px">Log Disabled Cinder Service Information</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-services/disable-log-reason?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableACinderService(projectId, body, callback)</td>
    <td style="padding:15px">Enable a Cinder Service</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-services/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentLogLevelsForCinderServices(projectId, body, callback)</td>
    <td style="padding:15px">Get Current Log Levels for Cinder Services</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-services/get-log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setLogLevelsOfCinderServicesDynamically(projectId, body, callback)</td>
    <td style="padding:15px">Set Log Levels of Cinder Services Dynamically</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-services/set-log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">freezeACinderBackendHost(projectId, body, callback)</td>
    <td style="padding:15px">Freeze a Cinder Backend Host</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-services/freeze?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">thawACinderBackendHost(projectId, body, callback)</td>
    <td style="padding:15px">Thaw a Cinder Backend Host</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-services/thaw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">failoverACinderBackendHost(projectId, body, callback)</td>
    <td style="padding:15px">Failover a Cinder Backend Host</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-services/failover_host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroups(allTenants, sort, limit, offset, marker, projectId, callback)</td>
    <td style="padding:15px">List groups</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(projectId, body, callback)</td>
    <td style="padding:15px">Create group</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showGroupDetails(projectId, groupId, listVolume, callback)</td>
    <td style="padding:15px">Show group details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroup(projectId, groupId, body, callback)</td>
    <td style="padding:15px">Update group</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroupFromSource(projectId, body, callback)</td>
    <td style="padding:15px">Create group from source</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/groups/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(projectId, groupId, body, callback)</td>
    <td style="padding:15px">Delete group</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/groups/{pathv2}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupsWithDetails(allTenants, sort, limit, offset, marker, projectId, listVolume, callback)</td>
    <td style="padding:15px">List groups with details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/groups/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupSnapshot(projectId, groupSnapshotId, callback)</td>
    <td style="padding:15px">Delete group snapshot</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_snapshots/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showGroupSnapshotDetails(projectId, groupSnapshotId, callback)</td>
    <td style="padding:15px">Show group snapshot details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_snapshots/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupSnapshotsWithDetails(allTenants, sortKey, sortDir, limit, offset, marker, projectId, callback)</td>
    <td style="padding:15px">List group snapshots with details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_snapshots/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupSnapshots(allTenants, sortKey, sortDir, limit, offset, marker, projectId, callback)</td>
    <td style="padding:15px">List group snapshots</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroupSnapshot(projectId, body, callback)</td>
    <td style="padding:15px">Create group snapshot</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetGroupSnapshotStatus(projectId, groupSnapshotId, body, callback)</td>
    <td style="padding:15px">Reset group snapshot status</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_snapshots/{pathv2}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupType(projectId, groupTypeId, body, callback)</td>
    <td style="padding:15px">Update group type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showGroupTypeDetails(projectId, groupTypeId, callback)</td>
    <td style="padding:15px">Show group type details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupType(groupTypeId, projectId, callback)</td>
    <td style="padding:15px">Delete group type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDefaultGroupTypeDetails(projectId, callback)</td>
    <td style="padding:15px">Show default group type details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupTypes(sort, limit, offset, marker, projectId, callback)</td>
    <td style="padding:15px">List group types</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroupType(projectId, body, callback)</td>
    <td style="padding:15px">Create group type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateGroupSpecsForAGroupType(projectId, groupTypeId, body, callback)</td>
    <td style="padding:15px">Create or update group specs for a group type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types/{pathv2}/group_specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupSpecsForAGroupType(projectId, groupTypeId, callback)</td>
    <td style="padding:15px">List group specs for a group type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types/{pathv2}/group_specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showOneSpecificGroupSpecForAGroupType(projectId, groupTypeId, specId, callback)</td>
    <td style="padding:15px">Show one specific group spec for a group type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types/{pathv2}/group_specs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOneSpecificGroupSpecForAGroupType(projectId, groupTypeId, specId, body, callback)</td>
    <td style="padding:15px">Update one specific group spec for a group type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types/{pathv2}/group_specs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOneSpecificGroupSpecForAGroupType(projectId, groupTypeId, specId, callback)</td>
    <td style="padding:15px">Delete one specific group spec for a group type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/group_types/{pathv2}/group_specs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllHostsForAProject(adminProjectId, callback)</td>
    <td style="padding:15px">List all hosts for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showHostDetailsForAProject(adminProjectId, hostName, callback)</td>
    <td style="padding:15px">Show Host Details for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-hosts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAbsoluteLimitsForProject(projectId, callback)</td>
    <td style="padding:15px">Show absolute limits for project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMessage(projectId, messageId, callback)</td>
    <td style="padding:15px">Delete message</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/messages/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showMessageDetails(projectId, messageId, callback)</td>
    <td style="padding:15px">Show message details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/messages/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMessages(sort, limit, offset, marker, projectId, callback)</td>
    <td style="padding:15px">List messages</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listResourceFilters(resource, projectId, callback)</td>
    <td style="padding:15px">List resource filters</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/resource_filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateAQoSSpecificationFromAllAssociations(projectId, qosId, callback)</td>
    <td style="padding:15px">Disassociate a QoS specification from all associations</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/qos-specs/{pathv2}/disassociate_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unsetKeysInAQoSSpecification(projectId, qosId, body, callback)</td>
    <td style="padding:15px">Unset keys in a QoS specification</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/qos-specs/{pathv2}/delete_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssociationsForAQoSSpecification(projectId, qosId, callback)</td>
    <td style="padding:15px">Get all associations for a QoS specification</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/qos-specs/{pathv2}/associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateQoSSpecificationWithAVolumeType(volTypeId, projectId, qosId, callback)</td>
    <td style="padding:15px">Associate QoS specification with a volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/qos-specs/{pathv2}/associate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateQoSSpecificationFromAVolumeType(volTypeId, projectId, qosId, callback)</td>
    <td style="padding:15px">Disassociate QoS specification from a volume type</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/qos-specs/{pathv2}/disassociate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAQoSSpecificationDetails(projectId, qosId, callback)</td>
    <td style="padding:15px">Show a QoS specification details</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/qos-specs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setKeysInAQoSSpecification(projectId, qosId, body, callback)</td>
    <td style="padding:15px">Set keys in a QoS specification</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/qos-specs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAQoSSpecification(force, projectId, qosId, callback)</td>
    <td style="padding:15px">Delete a QoS specification</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/qos-specs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAQoSSpecification(projectId, body, callback)</td>
    <td style="padding:15px">Create a QoS specification</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/qos-specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listQoSSpecifications(sort, limit, marker, projectId, callback)</td>
    <td style="padding:15px">List QoS Specifications</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/qos-specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showQuotaClassesForAProject(quotaClassName, adminProjectId, callback)</td>
    <td style="padding:15px">Show quota classes for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-quota-class-sets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQuotaClassesForAProject(adminProjectId, quotaClassName, body, callback)</td>
    <td style="padding:15px">Update quota classes for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-quota-class-sets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showQuotasForAProject(usage, adminProjectId, projectId, callback)</td>
    <td style="padding:15px">Show quotas for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-quota-sets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQuotasForAProject(adminProjectId, projectId, body, callback)</td>
    <td style="padding:15px">Update quotas for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-quota-sets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteQuotasForAProject(projectId, adminProjectId, callback)</td>
    <td style="padding:15px">Delete quotas for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-quota-sets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultQuotasForAProject(projectId, adminProjectId, callback)</td>
    <td style="padding:15px">Get default quotas for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/os-quota-sets/{pathv2}/defaults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cleanupServices(projectId, body, callback)</td>
    <td style="padding:15px">Cleanup services</td>
    <td style="padding:15px">{base_path}/{version}/v3/{pathv1}/workers/cleanup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
